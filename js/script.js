let borderLoader = document.querySelector('.borderLoader');

window.addEventListener('load', () => {
    borderLoader.classList.add('hide');
    setTimeout(() => {
        borderLoader.remove();
    }),600;
})

const   controls = document.querySelectorAll('.controls'),
        slides = document.querySelectorAll('#slides .slide'),
        next = document.getElementById('next'),
        previous = document.getElementById('previous');

let count = 0;

controls.forEach(function(e) {
    e.style.display = 'inline-block';
});

function nextSlide(){
    goToSlide(count + 1);
}

function previousSlide(){
    goToSlide(count - 1);
}

function goToSlide(n){
    slides[count].className = 'slide';
    count = (n + slides.length) % slides.length;
    slides[count].className = 'slide showing';
}

next.onclick = function(){
    nextSlide();
};

previous.onclick = function(){
    previousSlide();
};
